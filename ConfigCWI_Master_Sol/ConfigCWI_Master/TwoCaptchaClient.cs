﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRISMDataAdaptor
{
    using System.IO;
    using System;
    using System.Net;
    using System.Text;
    using System.Threading;
    public enum ProxyType
    {
        HTTP,
        HTTPS,
        SOCKS4,
        SOCKS5
    }
    class TwoCaptchaClient
    {
        /// <summary>
        /// Sends a solve request and waits for a response
        /// </summary>
        /// <param name="Site_key">The "sitekey" value from site your captcha is located on</param>
        /// <param name="Captcha_Service_Key">The "Captcha_Service_Key" is your subscription key taken from 2 captcha</param>
        /// <param name="Captcha_Url">The page the captcha is located on</param>
        /// <param name="ProxyIP_Port">The proxy used, format: "username:password@ip:port</param>
        /// <param name="ProxyMode">The type of proxy used</param>

        public string Captcha_Service_Key { get; set; }
        public string Site_key { get; set; }
        public string TwoCaptcha_Url { get; set; }
        public string TwoCaptcha_Token_Url { get; set; }
        public string Captcha_Url { get; set; }
        public string ProxyIP_Port { get; set; }

        public ProxyType ProxyMode;

        public string GenerateCaptchaRequestID()
        {
            string gcaptchaToken = string.Empty;
            string requestUrl = string.Empty;
            string proxyType = string.Empty;
            try
            {
                ServicePointManager.Expect100Continue = false;
                var request = (HttpWebRequest)WebRequest.Create(TwoCaptcha_Url);

                switch (ProxyMode)
                {
                    case ProxyType.HTTP:
                        proxyType += "HTTP";
                        break;
                    case ProxyType.HTTPS:
                        proxyType += "HTTPS";
                        break;
                    case ProxyType.SOCKS4:
                        proxyType += "SOCKS4";
                        break;
                    case ProxyType.SOCKS5:
                        proxyType += "SOCKS5";
                        break;
                }

                if (string.IsNullOrEmpty(ProxyIP_Port))
                    requestUrl = "key=" + Captcha_Service_Key + "&method=userrecaptcha&googlekey=" + Site_key + "&pageurl=" + Captcha_Url;
                else
                {
                    if (string.IsNullOrEmpty(proxyType))
                    {
                        throw new Exception("Proxy Type is not set by user");
                    }
                    requestUrl = "key=" + Captcha_Service_Key + "&method=userrecaptcha&googlekey=" + Site_key + "&pageurl=" + Captcha_Url + "&proxy=" + ProxyIP_Port + "&proxytype=" + proxyType;
                }

                var requestData = Encoding.ASCII.GetBytes(requestUrl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = requestData.Length;
                using (var stream = request.GetRequestStream())
                    stream.Write(requestData, 0, requestData.Length);

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                response.Close();
                return responseString;
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occured in GenerateCaptchaRequestID. Error code:" + ex);
            }
        }

        public string GetCaptchaResponseKey(string captcha_id)
        {
            try
            {
                WebClient webClient = new WebClient();
                webClient.QueryString.Add("key", Captcha_Service_Key);
                webClient.QueryString.Add("action", "get");
                webClient.QueryString.Add("id", captcha_id);
                return webClient.DownloadString(TwoCaptcha_Token_Url);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occured in GetCaptchaResponseKey. Error code:" + ex);
            }
        }
    }
}
