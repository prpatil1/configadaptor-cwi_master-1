﻿namespace PRISMDataAdaptor
{
    using System.Collections.Generic;

    /// <summary>
    /// Stores all the required information of columns of the result table
    /// </summary>
    public class ResultTableColumns
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultTableColumns"/> class.
        /// </summary>
        public ResultTableColumns()
        {
            this.Name = string.Empty;
            this.Verify = string.Empty;
            this.Checks = new List<Check>();
            this.Logic = string.Empty;
            this.SatisfiesChecks = false;
            this.Clickable = false;
            this.Format = string.Empty;
            this.MapTo = string.Empty;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the verify.
        /// </summary>
        /// <value>
        /// The verify.
        /// </value>
        public string Verify { get; set; }

        /// <summary>
        /// Gets or sets the checks.
        /// </summary>
        /// <value>
        /// The checks.
        /// </value>
        public List<Check> Checks { get; set; }

        /// <summary>
        /// Gets or sets the logic.
        /// </summary>
        /// <value>
        /// The logic.
        /// </value>
        public string Logic { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [satisfies checks].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [satisfies checks]; otherwise, <c>false</c>.
        /// </value>
        public bool SatisfiesChecks { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ResultTableColumns"/> is clickable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if clickable; otherwise, <c>false</c>.
        /// </value>
        public bool Clickable { get; set; }

        /// <summary>
        /// Gets or Sets the Format
        /// </summary>
        /// <value>
        /// The Format Specified
        /// </value>
        public string Format { get; set; }

        /// <summary>
        /// Gets or Sets the MapTo for enabling mapping of value to corresponding field in Result
        /// </summary>
        public string MapTo { get; set; }
    }
}
