﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.IO;
using DeathByCaptcha;
using log4net;

public class CaptchaAutomation
{
    private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public string GetCaptcha(string userName, string password, string vstrCaptchaPollingTime, string vstrCaptchaExpiryTime, byte[] image, string parentVendor, ref string vstrErrorMsg)
    {
        log4net.Config.XmlConfigurator.Configure();

        string functionReturnValue = null;

        // Put your DBC username & password here:
        System.DateTime mdtTimerStartTime;
        int mintFirstLoop = 1;
        SocketClient clnt = default(SocketClient);
        double mdAccountBalance = 0;
        Captcha cptch = null;
        Captcha CaptchaDetails = null;
        vstrErrorMsg = string.Empty;
        functionReturnValue = string.Empty;
        try
        {
            logger.Info("DeatchByCaptcha Call made by ParentVendor: " + parentVendor);
            //Dim clnt As New SocketClient(userName, password)
            clnt = new SocketClient(userName, password);
            mdAccountBalance = clnt.GetBalance();

            // Put your CAPTCHA image file name, file object, stream, or vector of bytes here:
            cptch = clnt.Upload(image);
            mdtTimerStartTime = DateTime.Now;


            //Uploads a CAPTCHA to the DBC service for solving, returns uploaded CAPTCHA details on success, NULL otherwise
            if (cptch == null)
            {
                throw new System.Exception("Some problem occured while uploading CAPTCHA");
            }
            //*****Return Captcha if Captcha is resolved & has Value
            if (cptch.Solved && !string.IsNullOrWhiteSpace(cptch.Text))
            {
                return cptch.Text;
            }

            // Poll for the CAPTCHA status.
            while (cptch.Uploaded & !cptch.Solved)
            {
                if (mdtTimerStartTime.AddMilliseconds(Convert.ToDouble(vstrCaptchaExpiryTime) * 1000) > DateTime.Now)
                {
                    if (mintFirstLoop > 1)
                    {
                        Thread.Sleep(Client.PollsInterval[0] * Convert.ToInt32(vstrCaptchaPollingTime));
                    }
                    else if (mintFirstLoop == 1)
                    {
                        mintFirstLoop = mintFirstLoop + 1;

                    }
                    //cptch = clnt.GetCaptcha(cptch.Id)
                    CaptchaDetails = clnt.GetCaptcha(cptch.Id);
                    if (CaptchaDetails == null)
                    {
                        throw new System.Exception("Some problem occured while fetching uploaded Captcha Details");
                    }

                    if (CaptchaDetails.Solved)
                    {
                        // SaveFile(string.ConfigCWIFormat("CAPTCHA Received : " + CaptchaDetails.ToString() + ", Received Time : " + DateTime.Now.ToString), "DeathByCaptchaLog_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", true, vlngScreeningID);
                        //Return cptch.Text
                        functionReturnValue = CaptchaDetails.Text;
                        break; // TODO: might not be correct. Was : Exit While
                    }
                }
                else
                {
                    vstrErrorMsg = "Captcha Timeout";
                    break;
                }
            }
        }
        catch (AccessDeniedException ex)
        {
            logger.Error("DeatchByCaptcha Call made by ParentVendor: " + parentVendor, ex);
            vstrErrorMsg = "Access to DBC API denied, check your credentials and/or balance. " + ex.ToString();
        }
        catch (System.Exception ex)
        {
            logger.Error("DeatchByCaptcha Call made by ParentVendor: " + parentVendor, ex);
            vstrErrorMsg = ex.ToString();
        }
        return functionReturnValue;
    }
}
