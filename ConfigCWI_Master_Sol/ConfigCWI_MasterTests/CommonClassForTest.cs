﻿using PRISM.Common.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigCWI_MasterTests
{
    public static class CommonFunctions
    {
        internal static List<ConfigurableCWIStepsModel> ConfigurableCWISteps(string excelPath, int pageCount)
        {
            string sheetName = "Sheet1";
            DataTable table = ReadFromExcel(excelPath, pageCount, sheetName);
            return GetConfigurableCWISteps(table);
        }

        public static List<RuleEngineStepsModel> GetRuleEngineSteps(string excelPath, int pageCount)
        {
            string sheetName = "RuleEngineConfiguration";
            DataTable table = ReadFromExcel(excelPath, pageCount, sheetName);
            return GetRuleEngineSteps(table);
        }

        public static List<SourceFieldMappingModel> GetMappingDetails(string excelPath, int pageCount)
        {
            string sheetName = "Mapping";
            DataTable table = ReadFromExcel(excelPath, pageCount, sheetName);
            return GetMappingData(table);
        }

        public static List<DataFiltersModel> GetFilters(string excelPath, int pageCount)
        {
            string sheetName = "Filters";
            DataTable table = ReadFromExcel(excelPath, pageCount, sheetName);
            return GetFilterData(table);
        }

        public static List<ExtractionStepsModel> GetExtractionSteps(string excelPath, int pageCount)
        {
            string sheetName = "ExtractionModel";
            DataTable table = ReadFromExcel(excelPath, pageCount, sheetName);
            return GetExtractionSteps(table);
        }

        public static List<FieldConfigurationModel> GetFieldConfigurations(string excelPath, int pageCount)
        {
            string sheetName = "FieldConfiguration";
            DataTable table = ReadFromExcel(excelPath, pageCount, sheetName);
            return GetFieldConfigData(table);
        }

        private static DataTable ReadFromExcel(string excelPath, int pageCount, string sheetName)
        {
            DataTable table;
            try
            {
                string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1;\"";
                ////Using a new Excel connection
                for (int j = 0; j < pageCount; j++)
                {
                    using (OleDbConnection conn = new OleDbConnection(connectionString))
                    {
                        conn.Open();
                        table = new DataTable();
                        using (DataTable dtExcelSchema = conn.GetSchema("Tables"))
                        {
                            if (dtExcelSchema.Rows[j]["TABLE_NAME"].ToString().Equals("_xlnm#_FilterDatabase")) { pageCount++; continue; }
                            if (sheetName.Equals(dtExcelSchema.Rows[j]["TABLE_NAME"].ToString().Replace("$", "")))
                            {
                                string query = "SELECT * FROM [" + dtExcelSchema.Rows[j]["TABLE_NAME"].ToString() + "]";
                                OleDbDataAdapter adapter = new OleDbDataAdapter(query, conn);
                                adapter.Fill(table);
                                return table;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static List<ConfigurableCWIStepsModel> GetConfigurableCWISteps(DataTable table)
        {
            List<ConfigurableCWIStepsModel> list = new List<ConfigurableCWIStepsModel>();
            try
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    list.Add(new ConfigurableCWIStepsModel()
                    {
                        ParentVendor = table.Rows[i]["ParentVendor"].ToString(),
                        SequenceNumber = int.Parse(table.Rows[i]["SequenceNumber"].ToString()),
                        StepDescription = table.Rows[i]["StepDescription"].ToString(),
                        Step = table.Rows[i]["Step"].ToString(),
                        StepType = table.Rows[i]["StepType"].ToString(),
                        ConfigCWIParameter = table.Rows[i]["Parameters"].ToString(),
                        Data = table.Rows[i]["Data"].ToString(),
                        Verify = table.Rows[i]["Verify"].ToString(),
                        Clickable = table.Rows[i]["Clickable"].ToString(),
                        Logic = table.Rows[i]["Logic"].ToString(),
                        ConfigCWIFormat = table.Rows[i]["Format"].ToString(),
                        MapTo = table.Rows[i]["MapTo"].ToString()
                    });
                }
            }
            catch (Exception ex) { throw ex; }
            return list;
        }

        public static List<RuleEngineStepsModel> GetRuleEngineSteps(DataTable table)
        {
            List<RuleEngineStepsModel> list = new List<RuleEngineStepsModel>();
            try
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    list.Add(new RuleEngineStepsModel()
                    {
                        SequenceNumber = int.Parse(table.Rows[i]["SequenceNumber"].ToString()),
                        Description = table.Rows[i]["Description"].ToString(),
                        Step = table.Rows[i]["Step"].ToString(),
                        StepType = table.Rows[i]["StepType"].ToString(),
                        Parameter = table.Rows[i]["Parameter"].ToString(),
                        InputData = table.Rows[i]["InputData"].ToString(),
                        FieldName = table.Rows[i]["FieldName"].ToString(),
                        Priority = table.Rows[i]["Priority"].ToString()
                    });
                }
            }
            catch (Exception ex) { throw ex; }
            return list;
        }

        public static List<SourceFieldMappingModel> GetMappingData(DataTable table)
        {
            List<SourceFieldMappingModel> list = new List<SourceFieldMappingModel>();
            try
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    list.Add(new SourceFieldMappingModel()
                    {
                        Description = table.Rows[i]["Description"].ToString(),
                        SectionName = table.Rows[i]["SectionName"].ToString(),
                        WebColumnName = table.Rows[i]["WebColumnName"].ToString(),
                        MappingField = table.Rows[i]["MappingField"].ToString()
                    });
                }
            }
            catch (Exception ex) { throw ex; }
            return list;
        }

        public static List<DataFiltersModel> GetFilterData(DataTable table)
        {
            List<DataFiltersModel> list = new List<DataFiltersModel>();
            try
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    list.Add(new DataFiltersModel()
                    {
                        Description = table.Rows[i]["Description"].ToString(),
                        WebColumnName = table.Rows[i]["WebColumnName"].ToString(),
                        FilterText = table.Rows[i]["FilterText"].ToString(),
                        Condition = table.Rows[i]["Condition"].ToString(),
                        MatchType = table.Rows[i]["MatchType"].ToString(),
                        ConditionLevel = table.Rows[i]["ConditionLevel"].ToString()
                    });
                }
            }
            catch (Exception ex) { throw ex; }
            return list;
        }

        public static List<ExtractionStepsModel> GetExtractionSteps(DataTable table)
        {
            List<ExtractionStepsModel> list = new List<ExtractionStepsModel>();
            try
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    list.Add(new ExtractionStepsModel()
                    {
                        ActionName = table.Rows[i]["ActionName"].ToString(),
                        ActionType = table.Rows[i]["ActionType"].ToString(),
                        SectionName = table.Rows[i]["SectionName"].ToString(),
                        ExtractionType = table.Rows[i]["ExtractionType"].ToString(),
                        ResponseName = table.Rows[i]["ResponseName"].ToString(),
                        InputData = table.Rows[i]["InputData"].ToString(),
                        Mandatory = table.Rows[i]["Mandatory"].ToString(),
                        SequenceNumber = int.Parse(table.Rows[i]["SequenceNumber"].ToString())
                    });
                }
            }
            catch (Exception ex) { throw ex; }
            return list;
        }

        public static List<FieldConfigurationModel> GetFieldConfigData(DataTable table)
        {
            List<FieldConfigurationModel> list = new List<FieldConfigurationModel>();
            try
            {
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    list.Add(new FieldConfigurationModel()
                    {
                        ColumnName = table.Rows[i]["ColumnName"].ToString(),
                        ColumnSequence = int.Parse(table.Rows[i]["ColumnSequence"].ToString()),
                        SectionName = table.Rows[i]["SectionName"].ToString(),
                        ColumnProperty = table.Rows[i]["ColumnProperty"].ToString()
                    });
                }
            }
            catch (Exception ex) { throw ex; }
            return list;
        }
    }

}
